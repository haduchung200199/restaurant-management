package file.handler;

import restaurant.Menu.FoodMenu;

import java.io.*;
import java.util.*;

/**
 * read file for FoodMenu
 */
public class FoodMenuFileHandler {
    //read file and return a Map of food menu
    public  HashMap<String,FoodMenu> readFile(String fileName) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new FileReader(fileName));
        //Map to store menu with key is foodName
        HashMap<String,FoodMenu> map = new HashMap<>();

        String data;
       while((data = bufferedReader.readLine())!=null){
           //an array that read file element with regex ","
           String[] array = data.split(", ");
           if(array.length == 4){
               //apply array element to menu
               FoodMenu foodMenu = new FoodMenu(array[0],array[1],Long.parseLong(array[2]),array[3]);
               //put element to hashmap
               map.put(foodMenu.foodName,foodMenu);
           }
       }
       //close reader
       bufferedReader.close();
       //return a map
       return map;
    }
    //write file for food menu
    public void writeFile(String fileName, HashMap<String,FoodMenu> map) throws IOException {
        FileWriter fileWriter = new FileWriter(fileName,false);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        // iterate map entries
        for (Map.Entry<String, FoodMenu> entry : map.entrySet()) {
            // put value separated by a colon
            bufferedWriter.write(String.valueOf(entry.getValue()));

            // new line
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }
}