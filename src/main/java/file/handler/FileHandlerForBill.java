package file.handler;

import restaurant.bill.Bill;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Read/write file for Bill
 */
public class FileHandlerForBill {
    //read file and return a Map of Bill
    public HashMap<Integer, Bill> readFile(String fileName) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        //Map to store menu with key is foodName
        HashMap<Integer, Bill> map = new HashMap<Integer, Bill>();
        List list = new LinkedList();
        String data;
        while ((data = bufferedReader.readLine()) != null) {
            //an array that read file element with regex ","
            String[] array = data.split("> ");
            if (array.length == 4) {
                list.add(array[1]);
                //apply array element to menu
                Bill bill = new Bill(Integer.parseInt(array[0]), list, array[2], Long.parseLong(array[3]));
                //put element to hashmap
                map.put(bill.billID, bill);
            }
        }
        //close reader
        bufferedReader.close();
        //return a map
        return map;
    }

    //write file contains Bill
    public void writeFile(String fileName, HashMap<Integer, Bill> map) throws IOException {
        File file = new File(fileName);
        FileWriter fileWriter = new FileWriter(file, false);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        // iterate map entries
        for (Map.Entry<Integer, Bill> entry : map.entrySet()) {
            // put value separated by a colon
            bufferedWriter.write(String.valueOf(entry.getValue()));

            // new line
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }
}
