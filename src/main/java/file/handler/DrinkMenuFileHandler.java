package file.handler;

import restaurant.Menu.DrinkMenu;

import java.io.*;
import java.util.*;

/**
 * read file for FoodMenu
 */
public class DrinkMenuFileHandler {
    //read file and return a Map of drink menu
    public HashMap<String, DrinkMenu> readFile(String fileName) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        //Map to store menu with key is foodName
        HashMap<String, DrinkMenu> map = new HashMap<String, DrinkMenu>();

        String data;
        while ((data = bufferedReader.readLine()) != null) {
            //an array that read file element with regex ","
            String[] array = data.split(", ");
            if (array.length == 4) {
                //apply array element to menu
                DrinkMenu menu = new DrinkMenu(array[0], array[1], Long.parseLong(array[2]), array[3]);
                //put element to hashmap
                map.put(menu.foodName, menu);
            }
        }
        //close reader
        bufferedReader.close();
        //return a map
        return map;
    }

    //write file contains drinks
    public static void writeFile(String fileName, HashMap<String, DrinkMenu> map) throws IOException {
        FileWriter fileWriter = new FileWriter(fileName, false);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        // iterate map entries
        for (Map.Entry<String, DrinkMenu> entry : map.entrySet()) {
            // put value separated by a colon
            bufferedWriter.write(String.valueOf(entry.getValue()));

            // new line
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }
}
