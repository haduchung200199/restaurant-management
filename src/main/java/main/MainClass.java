package main;

import option.menu.OptionMenu;

/**
 * main class
 */
public class MainClass {
    public static void main(String[] args) {
        OptionMenu menu = new OptionMenu();
        menu.showMenu();
    }
}
