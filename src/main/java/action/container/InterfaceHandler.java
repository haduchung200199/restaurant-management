package action.container;


import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * interface contains basic action for menu and bill
 */
public interface InterfaceHandler{
    public void insertNewItem(HashMap map) throws UnsupportedEncodingException; //insert
    public Boolean updateItem(String foodName, HashMap map) throws UnsupportedEncodingException; //update
    public Boolean deleteItem(String foodName, HashMap map); //delete
}
