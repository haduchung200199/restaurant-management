package action.container;

import restaurant.bill.Bill;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * handler payment for bill
 */
public interface BillHandler {
    /**
     * make payment with total price
     */
    long makePayment(long price,int quantity,long total);
    void insertBill(HashMap foodMap,HashMap drinkMap,HashMap billMap) throws UnsupportedEncodingException;
    Boolean updateBill(HashMap foodMap, HashMap drinkMap,HashMap<Integer,Bill> billMap, int billID) throws UnsupportedEncodingException;
    Boolean deleteBill(HashMap map, int billID);
}
