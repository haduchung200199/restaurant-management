package action.handler;

import action.container.InterfaceHandler;
import restaurant.Menu.FoodMenu;
import validator.InputValidator;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class to implement interfaceHandler
 */
public class FoodMenuHandler implements InterfaceHandler {
    @Override
    public void insertNewItem(HashMap map) throws UnsupportedEncodingException {
        String foodName;
        String menuType;
        long price;
        String description;

        //FoodMenu foodMenu = new FoodMenu();
        Scanner scanner = new Scanner(System.in);
        InputValidator inputValidator = new InputValidator();
        //input food name
        while(true) {
            System.out.println("Food name: ");
            foodName = scanner.nextLine();
            if (inputValidator.emptyOrNullCheck(foodName)) {
                System.out.println("Food name cant be empty");
                continue;
            }
            else if(inputValidator.checkExist(map,foodName)){
                System.out.println("Food Name is already exist!!");
                continue;
            }
            else if(inputValidator.hasNumberCheck(foodName)){
                System.out.println("Please do not input number");
                continue;
            }
            else break;
        }
        //input menu type
        int selection = 0;
        while(true) {
            System.out.println("Input your selection: ");
            System.out.println("1.Breakfast");
            System.out.println("2.Lunch");
            System.out.println("3.Dinner");
            System.out.println("4.Super");
            try{
                selection = scanner.nextInt();
                scanner.nextLine();
                if(inputValidator.emptyOrNullCheck(selection)){
                    System.out.println("Please input your selection!!");
                    continue;
                }
                else switch (selection){
                    case 1:{
                        menuType = "Breakfast";
                        break;
                    }
                    case 2:{
                        menuType = "Lunch";
                        break;
                    }
                    case 3: {
                        menuType = "Dinner";
                        break;
                    }
                    case 4:{
                        menuType = "Super";
                        break;
                    }
                    default:{
                        System.out.println("Please chosoe from 1 to 4");
                        continue;
                    }
                }
                break;
            }catch (InputMismatchException e){
                scanner.nextLine();
                System.out.println("Selection must only contains numbers");
            }
        }
        //input price
        while(true) {
            try {
                System.out.println("Price: ");
                price = scanner.nextLong();
                scanner.nextLine();
                break;
            }catch (InputMismatchException e){
                scanner.nextLine();
                System.out.println("Please input only numbers");
            }
        }
        //input description
        while(true) {
            System.out.println("Description: ");
            description = scanner.nextLine();
            if (inputValidator.emptyOrNullCheck(description)) {
                System.out.println("Food name cant be empty");
                continue;
            }
            else break;
        }
        FoodMenu menu = new FoodMenu(foodName,menuType,price,description);
        map.put(foodName,menu);
    }

    @Override
    public Boolean updateItem(String foodName, HashMap map) throws UnsupportedEncodingException {
        if(!map.containsKey(foodName)) return false;
        else{
            FoodMenu menu = new FoodMenu();
            String[] array = map.get(foodName).toString().split(", ");
            if(array.length == 4){
                //apply array element to menu
                menu = new FoodMenu(array[0],array[1],Long.parseLong(array[2]),array[3]);
            }
            System.out.println("Found Food: "+menu);
            String menuType;
            long price;
            String description;

            Scanner scanner = new Scanner(System.in);
            InputValidator inputValidator = new InputValidator();
            //input menu type
            int selection;
            while(true) {
                System.out.println("Input your selection: ");
                System.out.println("1.Breakfast");
                System.out.println("2.Lunch");
                System.out.println("3.Dinner");
                System.out.println("4.Super");
                try{
                    selection = scanner.nextInt();
                    scanner.nextLine();
                    switch (selection){
                        case 1:{
                            menuType = "Breakfast";
                            break;
                        }
                        case 2:{
                            menuType = "Lunch";
                            break;
                        }
                        case 3: {
                            menuType = "Dinner";
                            break;
                        }
                        case 4:{
                            menuType = "Super";
                            break;
                        }
                        default:{
                            System.out.println("Please chosoe from 1 to 4");
                            continue;
                        }
                    }
                    break;
                }catch (InputMismatchException e){
                    scanner.nextLine();
                    System.out.println("Selection must only contains numbers");
                }
            }
            //input price
            while(true) {
                try {
                    System.out.println("Price: ");
                    price = scanner.nextLong();
                    scanner.nextLine();
                    break;
                }catch (InputMismatchException e){
                    scanner.nextLine();
                    System.out.println("please input only numbers");
                }
            }
            //input description
            while(true) {
                System.out.println("Description: ");
                description = scanner.nextLine();
                if (inputValidator.emptyOrNullCheck(description)) {
                    description = menu.description;
                    System.out.println(description);
                    break;
                }
                else break;
            }
            map.put(foodName,new FoodMenu(foodName,menuType,price,description));
            return true;
        }
    }

    @Override
    public Boolean deleteItem(String foodName, HashMap map) {
        if(map.containsKey(foodName)){
            map.remove(foodName);
            return true;
        } else return false;
    }
}
