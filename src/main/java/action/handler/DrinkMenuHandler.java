package action.handler;

import action.container.InterfaceHandler;
import restaurant.Menu.DrinkMenu;
import restaurant.Menu.FoodMenu;
import validator.InputValidator;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class to handle Drink menu interface
 */
public class DrinkMenuHandler implements InterfaceHandler {
    @Override
    public void insertNewItem(HashMap map) throws UnsupportedEncodingException {
        String foodName;
        String drinkType;
        long price;
        String description;
        FoodMenu foodMenu = new FoodMenu();
        Scanner scanner = new Scanner(System.in);
        InputValidator inputValidator = new InputValidator();
        //input drink name
        while(true) {
            System.out.println("Drink name: ");
            foodName = scanner.nextLine();
            if (inputValidator.emptyOrNullCheck(foodName)) {
                System.out.println("Drink name cant be empty");
                continue;
            }
            else if(inputValidator.checkExist(map,foodName)){
                System.out.println("Drink Name is already exist!!");
                continue;
            }
            else if(inputValidator.hasNumberCheck(foodName)){
                System.out.println("Please do not input number");
                continue;
            }
            else break;
        }
        //input drink type
        int selection = 0;
        while(true) {
            System.out.println("Input your selection: ");
            System.out.println("1.Soft drink");
            System.out.println("2.Juice");
            System.out.println("3.Alcohol");
            System.out.println("4.smoothie");
            try{
                selection = scanner.nextInt();
                scanner.nextLine();
                if(inputValidator.emptyOrNullCheck(selection)){
                    System.out.println("Please input your selection!!");
                    continue;
                }
                else switch (selection){
                    case 1:{
                        drinkType = "Soft drink";
                        break;
                    }
                    case 2:{
                        drinkType = "Juice";
                        break;
                    }
                    case 3: {
                        drinkType = "Alcohol";
                        break;
                    }
                    case 4:{
                        drinkType = "Smoothie";
                        break;
                    }
                    default:{
                        System.out.println("Please chosoe from 1 to 4");
                        continue;
                    }
                }
                break;
            }catch (InputMismatchException e){
                scanner.nextLine();
                System.out.println("Selection must only contains numbers");
            }
        }
        //input price
        while(true) {
            try {
                System.out.println("Price: ");
                price = scanner.nextLong();
                scanner.nextLine();
                break;
            }catch (InputMismatchException e){
                scanner.nextLine();
                System.out.println("Please input only numbers!!");
            }
        }
        //input description
        while(true) {
            System.out.println("Description: ");
            description = scanner.nextLine();
            if (inputValidator.emptyOrNullCheck(description)) {
                System.out.println("Food name cant be empty");
                continue;
            }
            else break;
        }
        map.put(foodName,new FoodMenu(foodName,drinkType,price,description));
    }
    @Override
    public Boolean updateItem(String foodName,HashMap map) throws UnsupportedEncodingException {
        if(!map.containsKey(foodName)) return false;
        else{
            DrinkMenu menu = new DrinkMenu();
            String[] array = map.get(foodName).toString().split(", ");
            if(array.length == 4){
                //apply array element to menu
                menu = new DrinkMenu(array[0],array[1],Long.parseLong(array[2]),array[3]);
            }
            System.out.println("Found Drink: "+menu);
            String drinkType;
            long price;
            String description;

            Scanner scanner = new Scanner(System.in);
            InputValidator inputValidator = new InputValidator();
            //input drink type
            int selection = 0;
            while(true) {
                System.out.println("Input your selection: ");
                System.out.println("1.Soft drink");
                System.out.println("2.Juice");
                System.out.println("3.Alcohol");
                System.out.println("4.smoothie");
                try{
                    selection = scanner.nextInt();
                    scanner.nextLine();
                    if(inputValidator.emptyOrNullCheck(selection)){
                        System.out.println("Please input your selection!!");
                        continue;
                    }
                    else switch (selection){
                        case 1:{
                            drinkType = "Soft drink";
                            break;
                        }
                        case 2:{
                            drinkType = "Juice";
                            break;
                        }
                        case 3: {
                            drinkType = "Alcohol";
                            break;
                        }
                        case 4:{
                            drinkType = "Smoothie";
                            break;
                        }
                        default:{
                            System.out.println("Please chosoe from 1 to 4");
                            continue;
                        }
                    }
                    break;
                }catch (InputMismatchException e){
                    scanner.nextLine();
                    System.out.println("Selection must only contains numbers");
                }
            }
            //input price
            while(true) {
                try {
                    System.out.println("Price: ");
                    price = scanner.nextLong();
                    scanner.nextLine();
                    break;
                }catch (InputMismatchException e){
                    scanner.nextLine();
                    System.out.println("Please input only numbers!!");
                }
            }
            //input description
            while(true) {
                System.out.println("Description: ");
                description = scanner.nextLine();
                if (inputValidator.emptyOrNullCheck(description)) {
                    description = menu.description;
                    System.out.println(description);
                    break;
                }
                else break;
            }
            map.put(foodName,new DrinkMenu(foodName,drinkType,price,description));
            return true;
        }
    }

    @Override
    public Boolean deleteItem(String foodName, HashMap map) {
        if(map.containsKey(foodName)){
            System.out.println("delete item: "+map.get(foodName));
            System.out.println();
            map.remove(foodName);
            return true;
        } else return false;
    }
}
