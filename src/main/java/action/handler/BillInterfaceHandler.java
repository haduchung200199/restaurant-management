package action.handler;

import action.container.BillHandler;
import restaurant.bill.Bill;
import restaurant.Menu.DrinkMenu;
import restaurant.Menu.FoodMenu;
import validator.InputValidator;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Handle Interface for Bill
 */
public class BillInterfaceHandler implements BillHandler {
    //make total payment for Bill
    @Override
    public long makePayment(long price, int quantity, long total) {
        total = total + price*quantity;
        return total;
    }
    //insert new Bill
    @Override
    public void insertBill(HashMap foodMap,HashMap drinkMap,HashMap billMap) throws UnsupportedEncodingException {
        int billID;
        List menuItems = new LinkedList<>();
        int quantity;
        String orderTime;
        //total of all the items of the bill
        long totalPrice = 0;
        String foodName;

        Scanner scanner = new Scanner(System.in);
        InputValidator inputValidator = new InputValidator();
        //billId will be random
        billID = new Random().nextInt(100000);
        //create a mini map for food menu.
        HashMap<String, Integer> miniFoodMap = new HashMap();
        //create a mini map for drink menu
        HashMap<String, Integer> miniDrinkMap = new HashMap();

        //input menu items
        while(true) {
            System.out.println("Input Food name: ");
            foodName = scanner.nextLine();
            if (inputValidator.emptyOrNullCheck(foodName)) {
                System.out.println("Food name cant be empty");
                continue;
            }
            //input a key to break while loop
            else if(foodName.equalsIgnoreCase("q")){
                break;
            }
            else if(inputValidator.hasNumberCheck(foodName)){
                System.out.println("Please do not input number");
                continue;
            }
            else if(!foodMap.containsKey(foodName) && !drinkMap.containsKey(foodName)){
                System.out.println("Food/Drink does not exist!!");
                continue;
            }

            //input Quantity
            while(true){
                try{
                    System.out.println("Input Quantity: ");
                    quantity = scanner.nextInt();
                    scanner.nextLine();
                    break;
//                    if(inputValidator.emptyOrNullCheck(quantity)){
//                        System.out.println("Please input quantity!!!");
//                        //continue;
//                    }else break;

                }catch (InputMismatchException e){
                    scanner.nextLine();
                    System.out.println("Please only input numbers!!!");
                }
            }

            if(foodMap.containsKey(foodName)){
                FoodMenu foodMenu = new FoodMenu();
                String[] array = foodMap.get(foodName).toString().split(", ");
                if(array.length == 4){
                    //apply array element to menu
                    foodMenu = new FoodMenu(array[0],array[1],Long.parseLong(array[2]),array[3]);
                }
                totalPrice = makePayment(foodMenu.price,quantity,totalPrice);

                //if input duplicate food/drink -> add more to quantity
                if(miniFoodMap.containsKey(foodName)){
                    quantity = quantity + (miniFoodMap.get(foodName));
                        menuItems.clear();
                        miniFoodMap.remove(foodName);
                }
                miniFoodMap.put(foodName,quantity);
                menuItems.add(miniFoodMap);

            }
            else {
                DrinkMenu drinkMenu = new DrinkMenu();
                String[] array = drinkMap.get(foodName).toString().split(", ");
                if(array.length == 4){
                    //apply array element to menu
                    drinkMenu = new DrinkMenu(array[0],array[1],Long.parseLong(array[2]),array[3]);
                }
                totalPrice = makePayment(drinkMenu.price,quantity,totalPrice);

                if(miniDrinkMap.containsKey(foodName)){
                    quantity = quantity + (miniDrinkMap.get(foodName));
                    menuItems.clear();
                    miniDrinkMap.remove(foodName);
                }
                miniDrinkMap.put(foodName,quantity);
                menuItems.add(miniDrinkMap);
            }
        }
        //get current date time
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
        LocalDateTime now = LocalDateTime.now();
        orderTime =  dateTimeFormatter.format(now);
        Bill bill = new Bill(billID,menuItems,orderTime,totalPrice);
        billMap.put(billID,bill);

    }
    //update Bill using billID to seek for Bill.
    @Override
    public Boolean updateBill(HashMap foodMap, HashMap drinkMap,HashMap<Integer,Bill> billMap, int billID) throws UnsupportedEncodingException { ;
        if(billMap.containsKey(billID)){
            String dateTime;
            long totalPrice=0;
            int quantity;
            String foodName;

            //mini map for Food menu
            HashMap<String,Integer> miniFoodMap = new HashMap();
            //mini map for Drink menu
            HashMap<String,Integer> miniDrinkMap = new HashMap();

            Bill bill = new Bill();
            //this list is for storing bill's menuItems
            List menuItems = new LinkedList();

            InputValidator inputValidator = new InputValidator();
            Scanner scanner = new Scanner(System.in);

            //get bill from billMap
            String[] array = billMap.get(2).toString().split("> ");
            String items="" ;
            if(array.length == 4){
                for(int runVariable = 2; runVariable<array[1].length()-2;runVariable++){
                    items = items + array[1].charAt(runVariable);
                }
                //menuItems.add(array[1]);
                menuItems.add(items);
                System.out.println(menuItems);
                //apply array element to menu
                bill = new Bill(Integer.parseInt(array[0]), menuItems, array[2], Long.parseLong(array[3]));
            }
            //print found Bill
            System.out.println(bill);

            //input menu items
            while(true) {
                System.out.println("Input Food name: ");
                foodName = scanner.nextLine();
                if (inputValidator.emptyOrNullCheck(foodName)) {
                    System.out.println("Food name cant be empty");
                    continue;
                }
                //input a key to break while loop
                else if (foodName.equalsIgnoreCase("q")) {
                    break;
                } else if (inputValidator.hasNumberCheck(foodName)) {
                    System.out.println("Please do not input number");
                    continue;
                } else if (!foodMap.containsKey(foodName) && !drinkMap.containsKey(foodName)) {
                    System.out.println("Food/Drink does not exist!!");
                    continue;
                }

                //input Quantity
                while (true) {
                    try {
                        System.out.println("Input Quantity: ");
                        quantity = scanner.nextInt();
                        scanner.nextLine();
                        break;
//                    if(inputValidator.emptyOrNullCheck(quantity)){
//                        System.out.println("Please input quantity!!!");
//                        //continue;
//                    }else break;

                    } catch (InputMismatchException e) {
                        scanner.nextLine();
                        System.out.println("Please only input numbers!!!");
                    }
                }
                if (foodMap.containsKey(foodName)) {
                    FoodMenu foodMenu = new FoodMenu();
                    //get menu from Food Menu.
                    String[] array1 = foodMap.get(foodName).toString().split(", ");
                    if (array.length == 4) {
                        //apply array element to menu
                        foodMenu = new FoodMenu(array1[0], array1[1], Long.parseLong(array1[2]), array1[3]);
                    }
                    //calculate total price
                    totalPrice = makePayment(foodMenu.price, quantity, totalPrice);

                    //if selected FoodName is already added before, quantity = quantity + new input quantity
                    if (miniFoodMap.containsKey(foodName)) {
                        quantity = quantity + (miniFoodMap.get(foodName));
                        menuItems.remove(miniFoodMap.get(foodName.toString()));
                        miniFoodMap.remove(foodName);
                    }
                    if(miniFoodMap.isEmpty()) menuItems.clear();
                    miniFoodMap.put(foodName, quantity);
                    menuItems.add(miniFoodMap);

                } else {
                    DrinkMenu drinkMenu = new DrinkMenu();
                    //get Menu from Drink menu
                    String[] array2 = drinkMap.get(foodName).toString().split(", ");
                    if (array.length == 4) {
                        //apply array element to menu
                        drinkMenu = new DrinkMenu(array2[0], array2[1], Long.parseLong(array2[2]), array2[3]);
                    }
                    totalPrice = makePayment(drinkMenu.price, quantity, totalPrice);

                    //if selected FoodName is already added before, quantity = quantity + new input quantity
                    if (miniDrinkMap.containsKey(foodName)) {
                        quantity = quantity + (miniDrinkMap.get(foodName));
                        menuItems.clear();
                        miniDrinkMap.remove(foodName);
                    }
                    if(miniFoodMap.isEmpty()) menuItems.clear();
                    miniDrinkMap.put(foodName, quantity);
                    menuItems.add(miniDrinkMap);
                }
            }
            dateTime = bill.orderTime;
            bill = new Bill(billID,menuItems,dateTime,totalPrice);
            System.out.println("new updated bill for #"+billID+"bill: "+bill);
            billMap.put(billID,bill);
            return true;
        }
        else return false;

    }
    //delete Bill using billID as key
    @Override
    public Boolean deleteBill(HashMap billMap, int billID) {
        if(billMap.containsKey(billID)) {
            billMap.remove(billID);
            return true;
        }
        else return false;

    }
}
