package validator;


import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * this class contains many validators for input
 */
public class InputValidator<T> {
    /**
     * check if input parameter is empty
     * @param parameter
     * @return
     */
    public Boolean emptyOrNullCheck(T parameter){
        //null -> return true
        if(parameter == null ) return true;
        //empty string -> true.
        //else false
        else return parameter.toString().trim().isEmpty();
    }

    /**
     * check if input is already exist
     * @param map
     * @param foodName
     * @return
     */
    public Boolean checkExist(HashMap map, String foodName){
        if(map.containsKey(foodName)) return true;
        else return false;
    }

    /**
     * check if String contains numbers
     * @param string
     * @return
     * @throws UnsupportedEncodingException
     */
    public Boolean hasNumberCheck(String string) throws UnsupportedEncodingException {
        byte asciiCheck[] = string.getBytes("US-ASCII");
        for(int i = 0;i<string.length();i++){
            if(asciiCheck[i]>=48 && asciiCheck[i]<=57){
                //contains number -> true
                return true;
            }
        }
        //else false
        return false;
    }

//    /**
//     * check if contains only number
//     * @param string
//     * @return
//     * @throws UnsupportedEncodingException
//     */
//    public Boolean isOnlynumberCheck(String string) throws UnsupportedEncodingException {
//        byte asciiCheck[] = string.getBytes("US-ASCII");
//        for(int i = 0;i<string.length();i++){
//            if(asciiCheck[i]<48 || asciiCheck[i]>57){
//                //not only numbers -> false
//                return false;
//            }
//        }
//        //only numbers - > true
//        return true;
//    }
}
