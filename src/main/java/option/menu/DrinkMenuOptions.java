package option.menu;

import file.handler.DrinkMenuFileHandler;
import restaurant.Menu.DrinkMenu;
import action.handler.DrinkMenuHandler;
import validator.InputValidator;

import java.io.IOException;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

public class DrinkMenuOptions extends MenuShower {
    @Override
    public void showMenu() throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);
        String drinkFile = "D:\\Drink.txt";
        DrinkMenu menu = new DrinkMenu();
        InputValidator inputValidator = new InputValidator();
        DrinkMenuFileHandler drinkMenuFileHandler = new DrinkMenuFileHandler();
        HashMap<String,DrinkMenu> map;
        map = drinkMenuFileHandler.readFile(drinkFile);
        //use flag to break while loop
        boolean flag = true;
        while(flag) {
            System.out.println("Please select what you want! : ");
            System.out.println("1.Insert new Item");
            System.out.println("2.Update Item");
            System.out.println("3.Delete Item");
            System.out.println("4.Show Drink Menu");
            System.out.println("5.Exit");
            int selection;
            try{
                selection = scanner.nextInt();
                scanner.nextLine();
                if(inputValidator.emptyOrNullCheck(selection)){
                    System.out.println("Selection can not be null: ");
                    continue;
                }
                switch (selection) {
                    case 1: {
                        DrinkMenuHandler drinkMenuHandler = new DrinkMenuHandler();
                        drinkMenuHandler.insertNewItem(map);
                        drinkMenuFileHandler.writeFile(drinkFile,map);
                        break;
                    }
                    case 2: {
                        String drinkName;
                        while(true){
                            System.out.println("Input foodName: ");
                            drinkName = scanner.nextLine();
                            if(inputValidator.emptyOrNullCheck(drinkName)){
                                System.out.println("Please input drink name!!!");
                                continue;
                            }
                            else if(inputValidator.hasNumberCheck(drinkName)){
                                System.out.println("Please do not input number");
                                continue;
                            }
                            else break;
                        }

                        DrinkMenuHandler drinkMenuHandler = new DrinkMenuHandler();
                        boolean result =  drinkMenuHandler.updateItem(drinkName,map);
                        if (result){
                            System.out.println("Update completed");
                            drinkMenuFileHandler.writeFile(drinkFile,map);
                        }
                        else System.out.println("Update failed, food name does not exist");
                        break;
                    }
                    case 3:{
                        String drinkName;
                        while(true){
                            System.out.println("Input name to delete");
                            drinkName = scanner.nextLine();
                            if(inputValidator.emptyOrNullCheck(drinkName)){
                                System.out.println("Please input food name!!!");
                                continue;
                            }
                            else if(inputValidator.hasNumberCheck(drinkName)){
                                System.out.println("Please do not input number");
                                continue;
                            }
                            else break;
                        }

                        DrinkMenuHandler drinkMenuHandler = new DrinkMenuHandler();
                        boolean result =  drinkMenuHandler.deleteItem(drinkName,map);
                        if (result){
                            System.out.println("Delete completed");
                            drinkMenuFileHandler.writeFile(drinkFile,map);
                        }
                        else System.out.println("Delete failed, food name does not exist");
                        break;
                    }
                    case 4:{
                        System.out.println("List of drinks");
                        System.out.println(map);
                        System.out.println();
                        break;
                    }
                    case 5:{
                        System.out.println("Exit drink menu!");
                        //break while loop with flag = false
                        flag = false;
                        break;
                    }
                    default: {
                        System.out.println("Please choose from 1 to 5");
                        continue;
                    }
                }
            }catch (InputMismatchException e){
                scanner.nextLine();
                System.out.println("Please input number only!!");
                continue;
            }
        }
    }
}
