package option.menu;

import file.handler.FoodMenuFileHandler;
import restaurant.Menu.FoodMenu;
import action.handler.FoodMenuHandler;
import validator.InputValidator;

import java.io.IOException;
import java.util.*;

public class FoodMenuOptions extends MenuShower {
    @Override
    public void showMenu() throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);
        String foodFile = "D:\\Food.txt";
        FoodMenu menu = new FoodMenu();
        InputValidator inputValidator = new InputValidator();
        FoodMenuFileHandler foodMenuFileHandler = new FoodMenuFileHandler();
        HashMap<String,FoodMenu> map;
        map = foodMenuFileHandler.readFile(foodFile);
        //use flag to break while loop
        boolean flag = true;
        while(flag) {
            System.out.println("Please select what you want! : ");
            System.out.println("1.Insert new Item");
            System.out.println("2.Update Item");
            System.out.println("3.Delete Item");
            System.out.println("4.Show Food Menu");
            System.out.println("5.Exit");
            int selection = 0;
            try{
                selection = scanner.nextInt();
                scanner.nextLine();
                if(inputValidator.emptyOrNullCheck(selection) || selection ==0){
                    System.out.println("Selection can not be null or 0!!! ");
                    continue;
                }
                switch (selection) {
                    case 1: {
                        FoodMenuHandler foodMenuHandler = new FoodMenuHandler();
                        foodMenuHandler.insertNewItem(map);
                        foodMenuFileHandler.writeFile(foodFile,map);
                        break;
                    }
                    case 2: {
                        String foodName;
                        while(true){
                            System.out.println("Input foodName: ");
                            foodName = scanner.nextLine();
                            if(inputValidator.emptyOrNullCheck(foodName)){
                                System.out.println("Please input food name!!!");
                                continue;
                            }
                            else if(inputValidator.hasNumberCheck(foodName)){
                                System.out.println("Please do not input number");
                                continue;
                            }
                            else break;
                        }

                        FoodMenuHandler foodMenuHandler = new FoodMenuHandler();
                        boolean result =  foodMenuHandler.updateItem(foodName,map);
                        if (result){
                            System.out.println("Update completed");
                            foodMenuFileHandler.writeFile(foodFile,map);
                        }
                        else System.out.println("Update failed, food name does not exist");

                        break;
                    }
                    case 3:{
                        String foodName;
                        while(true){
                            System.out.println("Input name to delete");
                            foodName = scanner.nextLine();
                            if(inputValidator.emptyOrNullCheck(foodName)){
                                System.out.println("Please input food name!!!");
                                continue;
                            }
                            else if(inputValidator.hasNumberCheck(foodName)){
                                System.out.println("Please do not input number");
                                continue;
                            }
                            else break;
                        }

                        FoodMenuHandler foodMenuHandler = new FoodMenuHandler();
                        boolean result =  foodMenuHandler.deleteItem(foodName,map);
                        if (result){
                            System.out.println("Delete completed");
                            foodMenuFileHandler.writeFile(foodFile,map);
                        }
                        else System.out.println("Delete failed, food name does not exist");
                        break;
                    }
                    case 4:{
                        System.out.println("List of Food: ");
                        for(Map.Entry<String,FoodMenu> entry: map.entrySet()){
                            System.out.println(entry.getValue());
                        }
                        System.out.println();
                        break;
                    }
                    case 5:{
                        System.out.println("Exit food menu!");
                        //break while loop with flag = false
                        flag = false;
                        break;
                    }
                    default: {
                        System.out.println("Please choose from 1 to 5");
                        continue;
                    }
                }
            }catch (InputMismatchException e){
                scanner.nextLine();
                System.out.println("Please input number only!!");
                continue;
            }
        }
    }
}
