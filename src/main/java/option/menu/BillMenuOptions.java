package option.menu;

import file.handler.FileHandlerForBill;
import file.handler.DrinkMenuFileHandler;
import file.handler.FoodMenuFileHandler;
import restaurant.bill.Bill;
import action.handler.BillInterfaceHandler;
import restaurant.Menu.DrinkMenu;
import restaurant.Menu.FoodMenu;
import validator.InputValidator;

import java.io.IOException;
import java.util.*;

public class BillMenuOptions extends MenuShower{
    @Override
    public void showMenu() throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);
        //filePath
        String billFile = "D:\\Bill.txt";
        String foodFile = "D:\\food.txt";
        String drinkFile = "D:\\drink.txt";
        InputValidator inputValidator = new InputValidator();
        //readFile bill file
        FileHandlerForBill fileHandlerForBill = new FileHandlerForBill();
        HashMap<Integer,Bill> billMap = fileHandlerForBill.readFile(billFile);
        //read food file
        FoodMenuFileHandler foodMenuFileHandler = new FoodMenuFileHandler();
        HashMap<String,FoodMenu> foodMap = foodMenuFileHandler.readFile(foodFile);
        //read drink file
        DrinkMenuFileHandler drinkMenuFileHandler = new DrinkMenuFileHandler();
        HashMap<String, DrinkMenu> drinkMap = drinkMenuFileHandler.readFile(drinkFile);
        //use flag to break while loop
        boolean flag = true;
        while(flag) {
            System.out.println("Please select what you want! : ");
            System.out.println("1.Insert new Item");
            System.out.println("2.Update Item");
            System.out.println("3.Delete Item");
            System.out.println("4.Show Bill");
            System.out.println("5.Exit");
            int selection;
            try{
                selection = scanner.nextInt();
                scanner.nextLine();

                switch (selection) {
                    //insert
                    case 1: {
                        BillInterfaceHandler billInterfaceHandler = new BillInterfaceHandler();
                        billInterfaceHandler.insertBill(foodMap,drinkMap,billMap);
                        fileHandlerForBill.writeFile(billFile,billMap);
                        break;
                    }
                    //update
                    case 2: {
                        int billID;
                        while(true) {
                            try {
                                System.out.println("Input billID: ");
                                billID = scanner.nextInt();
                                scanner.nextLine();
                                break;
                            }catch (InputMismatchException e){
                                scanner.nextLine();
                                System.out.println("Please only input numbers");
                                continue;
                            }
                        }

                        BillInterfaceHandler billInterfaceHandler = new BillInterfaceHandler();
                        boolean result =  billInterfaceHandler.updateBill(foodMap,drinkMap,billMap,billID);
                        if (result){
                            System.out.println("Update completed");
                            fileHandlerForBill.writeFile(billFile,billMap);
                        }
                        else System.out.println("Update failed, Bill ID  does not exist");

                        break;
                    }
                    //delete
                    case 3: {
                        int billId;
                        while (true) {
                            try {
                                System.out.println("Input Bill ID to delete");
                                billId = scanner.nextInt();
                                scanner.nextLine();
                                if (inputValidator.emptyOrNullCheck(billId)) {
                                    System.out.println("Please input billId!!!");
                                    continue;
                                } else break;
                            } catch (InputMismatchException e) {
                                scanner.nextLine();
                                System.out.println("Please only input numbers");
                                continue;
                            }
                        }
                        BillInterfaceHandler billInterfaceHandler = new BillInterfaceHandler();
                        boolean result = billInterfaceHandler.deleteBill(billMap, billId);
                        if (result) {
                            System.out.println("Delete completed");
                            fileHandlerForBill.writeFile(billFile, billMap);
                        } else System.out.println("Delete failed, Bill ID  does not exist");
                        break;
                    }
                    //show list Bill
                    case 4:{
                        System.out.println("List of Bill: ");
                        List menuItems = new LinkedList();
                        for(Map.Entry<Integer,Bill> entry: billMap.entrySet()){
                            System.out.println(entry.getValue());
                        }
                        break;
                    }
                    //exit
                    case 5:{
                        System.out.println("Exit Bill Management!");
                        //break while loop with flag = false
                        flag = false;
                        break;
                    }
                    default: {
                        System.out.println("Please choose from 1 to 5");
                        continue;
                    }
                }
            }catch (InputMismatchException e){
                scanner.nextLine();
                System.out.println("Please input number only!!");
                continue;
            }
        }
    }
}
