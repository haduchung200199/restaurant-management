package option.menu;

import java.io.IOException;

public abstract class MenuShower {
    public abstract void showMenu() throws IOException, ClassNotFoundException;
}
