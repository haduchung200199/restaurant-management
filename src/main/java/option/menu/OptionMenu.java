package option.menu;

import validator.InputValidator;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * menu to choose between FoodMenu or Drink Menu
 */
public class OptionMenu extends MenuShower{
    @Override
    public void showMenu() {
        Scanner scanner = new Scanner(System.in);
        InputValidator inputValidator = new InputValidator();
        boolean flag = true;
        while(flag){
            System.out.println("Please select what you want! : ");
            System.out.println("1.Food Menu");
            System.out.println("2.Drink Menu");
            System.out.println("3.Bill");
            System.out.println("4.Exit");
            int selection;
            try{
                selection = scanner.nextInt();
                scanner.nextLine();
                if(inputValidator.emptyOrNullCheck(selection)){
                    System.out.println("Selection can not be null: ");
                    continue;
                }
                switch (selection){
                    case 1:{
                        FoodMenuOptions foodMenuOptions = new FoodMenuOptions();
                        foodMenuOptions.showMenu();
                        break;
                    }
                    case 2:{
                        DrinkMenuOptions drinkMenuOptions = new DrinkMenuOptions();
                        drinkMenuOptions.showMenu();
                        break;
                    }
                    case 3:{
                        BillMenuOptions billMenuOptions  = new BillMenuOptions();
                        billMenuOptions.showMenu();
                        break;
                    }
                    case 4:{
                        flag = false;
                        System.out.println("Exit Option Menu!!");
                        break;
                    }
                    default:{
                        System.out.println("Please choose from 1 to 4");
                        continue;
                    }
                }
            }catch (InputMismatchException | IOException | ClassNotFoundException e){
                scanner.nextLine();
                System.out.println("Please just input number");
                continue;
            }
        }
    }
}
