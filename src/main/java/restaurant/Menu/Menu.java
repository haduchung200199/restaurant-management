package restaurant.Menu;

/**
 * class to manage menu
 */
public class Menu {
    public String foodName;
    public long price;
    public String description;

    //constructor with parameters
    public Menu(String foodName, long price, String description) {
        this.foodName = foodName;
        this.price = price;
        this.description = description;
    }
    //constructor without parameters
    public Menu() {
    }
    //getter and setter
    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
