package restaurant.Menu;

/**
 * class for food menu
 */
public class FoodMenu extends Menu {
    public String menuType; //breakfast, lunch, dinner
    //constructor with parameters
    public FoodMenu(String foodName, String menuType, long price, String description) {
        super(foodName, price, description);
        this.menuType = menuType;
    }
    //constructor without parameters
    public FoodMenu() {
    }
    //getter and setter
    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }
    //make param to string
    @Override
    public String toString() {
        return foodName + ", " + menuType+", "+ price+", "+description;
    }


}
