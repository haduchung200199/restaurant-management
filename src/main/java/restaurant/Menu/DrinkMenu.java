package restaurant.Menu;

/**
 * class for drink menu
 */
public class DrinkMenu extends Menu  {
    public String drinkType; //soft drink or alcohol or juice or smoothie
    //constructor with parameters
    public DrinkMenu(String foodName, String drinkType, long price, String description) {
        super(foodName, price, description);
        this.drinkType = drinkType;
    }
    //constructor without parameters
    public DrinkMenu() {
    }
    //getter and setter
    public String getDrinkType() {
        return drinkType;
    }

    public void setDrinkType(String drinkType) {
        this.drinkType = drinkType;
    }
    //make param to string

    @Override
    public String toString() {
        return foodName + ", " + drinkType+", "+ price+", "+description;
    }

}
