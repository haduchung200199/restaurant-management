package restaurant.bill;

import java.util.List;

public class Bill {
    public int billID;
    public List menuItems;
    public String orderTime;
    //total of all the items of the bill
    public long totalPrice;
    //constructor with variables
    public Bill(int billID,List menuItems,  String orderTime, long totalPrice) {
        this.billID = billID;
        this.menuItems = menuItems;
        this.orderTime = orderTime;
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return billID + "> " + menuItems + "> " + orderTime  + "> " + totalPrice;
    }

    //construsctor without variables
    public Bill() {
    }
    //getter and setter
    public List getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List menuItems) {
        this.menuItems = menuItems;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getBillID() {
        return billID;
    }

    public void setBillID(int billID) {
        this.billID = billID;
    }
}
