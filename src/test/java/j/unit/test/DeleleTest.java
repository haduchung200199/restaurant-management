package j.unit.test;

import file.handler.DrinkMenuFileHandler;
import file.handler.FileHandlerForBill;
import file.handler.FoodMenuFileHandler;
import restaurant.bill.Bill;
import action.handler.BillInterfaceHandler;
import restaurant.Menu.DrinkMenu;
import restaurant.Menu.FoodMenu;
import action.handler.DrinkMenuHandler;
import action.handler.FoodMenuHandler;
import org.testng.annotations.Test;
import static junit.framework.Assert.*;
import java.io.IOException;
import java.util.HashMap;

/**
 * test delete Methods
 */
public class DeleleTest {
    /**
     * test Delete method for FoodMenu
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Test
    public void testDeleteFoodMenuItems() throws IOException, ClassNotFoundException {
        FoodMenuHandler foodMenuHandler = new FoodMenuHandler();
        FoodMenuFileHandler foodMenuFileHandler = new FoodMenuFileHandler();
        //readfile to get Value for food map
        HashMap<String, FoodMenu> foodMap = foodMenuFileHandler.readFile("D:\\foodTest.txt");
        //before delete food menu item
        assertTrue(foodMap.containsKey("BeefSteak"));
        //delete food menu item
        Boolean result = foodMenuHandler.deleteItem("BeefSteak",foodMap);
        //after delete food menu item
        assertTrue(result);
        assertFalse(foodMap.containsKey("BeefSteak"));
    }
    /**
     * test Delete method for Drink Menu
     * @throws IOException
     */
    @Test
    public void testDeleteDrinkMenuItems() throws IOException {
        DrinkMenuFileHandler drinkMenuFileHandler = new DrinkMenuFileHandler();
        DrinkMenuHandler drinkMenuHandler = new DrinkMenuHandler();
        //readfile to get Value for drink map
        HashMap<String, DrinkMenu> drinkMap = drinkMenuFileHandler.readFile("D:\\drinkTest.txt");
        //before delete drink menu item
        assertTrue(drinkMap.containsKey("Apple Juice"));
        //delete drink menu item
        Boolean result = drinkMenuHandler.deleteItem("Apple Juice",drinkMap);
        //after delete drink menu item
        assertTrue(result);
        assertFalse(drinkMap.containsKey("Apple Juice"));
    }
    /**
     * test Delete method for Bill
     * @throws IOException
     */
    @Test
    public void testDeleteBillMenuItems() throws IOException {
        FileHandlerForBill fileHandlerForBill = new FileHandlerForBill();
        BillInterfaceHandler billInterfaceHandler = new BillInterfaceHandler();
        //readfile to get Value for bill
        HashMap<Integer, Bill> billMap = fileHandlerForBill.readFile("D:\\billTest.txt");
        //before delete bill item
        assertTrue(billMap.containsKey(2));
        //delete bill item
        Boolean result = billInterfaceHandler.deleteBill(billMap,2);
        //after delete bill item
        assertTrue(result);
        assertFalse(billMap.containsKey(2));
    }
}
