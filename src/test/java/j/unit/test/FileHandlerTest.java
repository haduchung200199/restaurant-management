package j.unit.test;

import file.handler.DrinkMenuFileHandler;
import file.handler.FileHandlerForBill;
import file.handler.FoodMenuFileHandler;
import restaurant.bill.Bill;
import restaurant.Menu.DrinkMenu;
import restaurant.Menu.FoodMenu;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.Assert.*;

/**
 * test read file for f
 */
public class FileHandlerTest {
    /**
     * test WriteFile to Food Menu
     * @throws IOException
     */
    @Test
    public void testWriteFileFoodMenu() throws IOException {
        //file path
        String fileName = "D:\\foodTest.txt";
        File file = new  File(fileName);
        HashMap<String,FoodMenu> foodMap = new HashMap();

        //add new foodMenu
        FoodMenu menu = new FoodMenu("BeefSteak","Lunch",200,"nice");
        //put food to map to write file
        foodMap.put("BeefSteak",menu);
        //write file
        FoodMenuFileHandler foodMenuFileHandler = new FoodMenuFileHandler();
        foodMenuFileHandler.writeFile(fileName,foodMap);
        //check if the file is written/created
        assertTrue(file.exists());
        assertTrue(file.isFile());
        assertTrue(file.canWrite());
        assertTrue(file.canRead());
    }
    /**
     * test the readFile method of FoodMenu
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Test
    public void testReadFileForFoodMenu() throws IOException, ClassNotFoundException {
        FoodMenuFileHandler foodMenuFileHandler = new FoodMenuFileHandler();
        //readFile food.txt to foodMap
        HashMap<String, FoodMenu> foodMap =  foodMenuFileHandler.readFile("D:\\foodTest.txt");
        //get values from map to foodMenu
        FoodMenu foodMenu = new FoodMenu();
        String[] array = foodMap.get("BeefSteak").toString().split(", ");
        if(array.length == 4) {
            //apply array element to menu
            foodMenu = new FoodMenu(array[0], array[1], Long.parseLong(array[2]), array[3]);
        }
        //set expected values to compare with values read from file
        FoodMenu foodMenuExpected = new FoodMenu("BeefSteak","Lunch",200,"nice");
        //assert values of foodMenuExpected with foodMenu read from file
        assertTrue(foodMenuExpected.foodName.equalsIgnoreCase(foodMenu.foodName));
        assertTrue(foodMenuExpected.menuType.equalsIgnoreCase(foodMenu.menuType));
        assertEquals(foodMenuExpected.price, foodMenu.price);
        assertTrue(foodMenuExpected.description.equalsIgnoreCase(foodMenu.description));
    }

    /**
     * test WriteFile to Food Menu
     * @throws IOException
     */
    @Test
    public void testWriteFileDrinkMenu() throws IOException {
        //file path
        String fileName = "D:\\drinkTest.txt";
        File file = new  File(fileName);
        HashMap<String,DrinkMenu> drinkMap = new HashMap();

        //add new foodMenu
        DrinkMenu menu = new DrinkMenu("Apple Juice","Juice",3000,"ok");
        //put food to map to write file
        drinkMap.put("Apple Juice",menu);
        //write file
        DrinkMenuFileHandler drinkMenuFileHandler = new DrinkMenuFileHandler();
        drinkMenuFileHandler.writeFile(fileName,drinkMap);
        //check if the file is written/created
        assertTrue(file.exists());
        assertTrue(file.isFile());
        assertTrue(file.canWrite());
        assertTrue(file.canRead());
    }

    /**
     * test readFile method of Drink menu
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @Test
    public void testReadFileDrinkMenu() throws IOException, ClassNotFoundException {
        DrinkMenuFileHandler drinkMenuFileHandler = new DrinkMenuFileHandler();
        //readFile drink.txt to drinkMap
        HashMap<String, DrinkMenu> drinkMap =  drinkMenuFileHandler.readFile("D:\\drinkTest.txt");
        //get values from map to drinkmenu
        DrinkMenu drinkMenu = new DrinkMenu();
        String[] array = drinkMap.get("Apple Juice").toString().split(", ");
        if(array.length == 4) {
            //apply array element to menu
             drinkMenu= new DrinkMenu(array[0], array[1], Long.parseLong(array[2]), array[3]);
        }
        //set expected values to compare with values read from file
        DrinkMenu drinkExpected = new DrinkMenu("Apple Juice","Juice",3000,"ok");
        //assert values of drinkMenuExpected with drinkMenu read from file
        assertTrue(drinkExpected.foodName.equalsIgnoreCase(drinkMenu.foodName));
        assertTrue(drinkExpected.drinkType.equalsIgnoreCase(drinkMenu.drinkType));
        assertEquals(drinkExpected.price, drinkMenu.price);
        assertTrue(drinkExpected.description.equalsIgnoreCase(drinkMenu.description));
    }

    /**
     * this method is for running with readFile For Bill test
     * @throws IOException
     */
    @Test
    public void testWriteFileBill() throws IOException {
        //file path
        String fileName = "D:\\billTest.txt";
        File file = new  File(fileName);
        HashMap<Integer,Bill> billMap = new HashMap();

        //add menuItems
        HashMap<String,Integer> expectedMap = new HashMap();
        expectedMap.put("Apple Juice",10);
        List menuItems = new LinkedList();
        menuItems.add(expectedMap);
        //add new Bill
        Bill bill = new Bill(2,menuItems,"17/06/2021 15:16",30000);
        //put bill to map to write file
        billMap.put(2,bill);
        //write file
        FileHandlerForBill fileHandlerForBill = new FileHandlerForBill();
        fileHandlerForBill.writeFile(fileName,billMap);
        //check if the file is written/created
        assertTrue(file.exists());
        assertTrue(file.isFile());
        assertTrue(file.canWrite());
        assertTrue(file.canRead());
    }

    /**
     * test readFile method of Bill
     */
    @Test
    public void testReadFileBill() throws IOException {
        FileHandlerForBill fileHandlerForBill = new FileHandlerForBill();
        //read file and apply values to billMap
        HashMap<Integer, Bill> billMap = fileHandlerForBill.readFile("D:\\billTest.txt");
        Bill bill = new Bill();
        //list to add menuItems
        List menuItems = new LinkedList();
        //apply values of Hasmap to bill
        String[] array = billMap.get(2).toString().split("> ");
        String items="" ; //this variable is used for getting String from List.
        if(array.length == 4){
            for(int runVariable = 2; runVariable<array[1].length()-2;runVariable++){
                items = items + array[1].charAt(runVariable);
            }
            //menuItems.add(array[1]);
            menuItems.add(items);
            //apply array element to menu
            bill = new Bill(Integer.parseInt(array[0]), menuItems, array[2], Long.parseLong(array[3]));
        }
        List listExpected = new LinkedList();
        HashMap<String,Integer> expectedMap = new HashMap();
        expectedMap.put("Apple Juice",10);
        listExpected.add(expectedMap);

        Bill expectedBill = new Bill(2,listExpected,"17/06/2021 15:16",30000);
        assertEquals(expectedBill.billID,bill.billID);
        assertTrue(expectedBill.menuItems.toString().equals(bill.menuItems.toString()));
        assertTrue(expectedBill.orderTime.equalsIgnoreCase(bill.orderTime));
        assertEquals(expectedBill.totalPrice,bill.totalPrice);

    }
}
