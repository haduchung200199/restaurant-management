package j.unit.test;

import validator.InputValidator;


import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import org.testng.annotations.Test;
import static junit.framework.Assert.*;

/**
 * test validator
 */
public class ValidatorTest {
    /**
     * test for emptyOrNullCheck method on true condition
     */
    @Test
    public void testEmptyOrNullCheck(){
        InputValidator inputValidator = new InputValidator();
        String emptyString = "";
        String nullString = null;
        assertTrue(inputValidator.emptyOrNullCheck(emptyString));
        assertTrue(inputValidator.emptyOrNullCheck(nullString));
    }

    /**
     * test for emptyOrNullCheck method on false condition
     */
    @Test
    public void testNotNull(){
        InputValidator inputValidator = new InputValidator();
        String notEmptyString = "Not empty or Null";
        assertFalse(inputValidator.emptyOrNullCheck(notEmptyString));
    }
    /**
     * test for hasNumberCheck method on true condition
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testIfStringHasNumber() throws UnsupportedEncodingException {
        InputValidator inputValidator = new InputValidator();
        String numberString = "abc123abc";
        assertTrue(inputValidator.hasNumberCheck(numberString));
    }

    /**
     * test for hasNumberCheck method on false condition
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testStringDoesNotHaveNumbers() throws UnsupportedEncodingException {
        InputValidator inputValidator = new InputValidator();
        String notNumberString = "abcxyz";
        assertFalse(inputValidator.hasNumberCheck(notNumberString));
    }

    /**
     * test for checkExist method on true condition
     */
    @Test
    public void testCheckExist(){
        InputValidator inputValidator = new InputValidator();
        HashMap<String,String> expectMap = new HashMap<>();
        expectMap.put("Food","Nice");
        assertTrue(inputValidator.checkExist(expectMap,"Food"));
    }

    /**
     * test for checkExist method on false condition
     */
    @Test
    public void testIfNotExist(){
        InputValidator inputValidator = new InputValidator();
        HashMap<String,String> expectMap = new HashMap<>();
        expectMap.put("Food","Nice");
        assertFalse(inputValidator.checkExist(expectMap,"Drink"));
    }
}
